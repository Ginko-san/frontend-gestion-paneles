FROM httpd:2.4
MAINTAINER Andres Garcia <jgarsalas@gmail.com>

## Write the app into Apache folder
COPY ./dist/ /usr/local/apache2/htdocs/
