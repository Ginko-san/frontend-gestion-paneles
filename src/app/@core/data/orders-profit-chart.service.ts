import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { OrdersChart, OrdersChartService } from './orders-chart.service';
import { ProfitChart, ProfitChartService } from './profit-chart.service';

export class OrderProfitChartSummary {
  title: string;
  value: number;
}

@Injectable()
export class OrdersProfitChartService {

  private summary = [
    {
      title: 'Total',
      value: 3654,
    },
    {
      title: 'Mes anterior',
      value: 946,
    },
    {
      title: 'Semana anterior',
      value: 654,
    },
    {
      title: 'Hoy',
      value: 230,
    },
  ];

  constructor(private ordersChartService: OrdersChartService,
              private profitChartService: ProfitChartService) {
  }

  getOrderProfitChartSummary(): Observable<OrderProfitChartSummary[]> {
    return observableOf(this.summary);
  }

  async getOrdersChartData(period: string, dispositivo:number){
    return (await this.ordersChartService.getOrdersChartData(period, dispositivo));
  }
  
}
