import { Injectable } from '@angular/core';
import { PeriodsService } from './periods.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';
import { AuthService } from '../../auth/auth.service';

export class OrdersChart {
  chartLabel: string[];
  linesData: number[][];
}

class DatosPaneles{
 // id:number;
  fecha: Date;
  power: any;
  deviceid : number;
  dispositivo_id: number; 
 // sistema_id : number;
 // sistema_nombre : string;
}

@Injectable()
export class OrdersChartService {
  toasterManagerService: any;
  constructor(private authService: AuthService, private period: PeriodsService,private http: HttpClient) {
  }

  private listaDatos : DatosPaneles[];
  private listaPower : number[][] = [];

  URL: string = `http://${environment.server}/`;
  httpClient: any;
  
 // fecha = new Date();
  annoActual = new Date().getFullYear();
  //mostraba un mes antes. Así que se le suma uno
  mesActual = new Date().getMonth()+1;
  diaActual = new Date().getDate();


  public anosAnteriores(){
    var years:string[] = [];
    var index = 6; 
    for(let i = 0;i<8;i++){
      if(i==7){
        years.push('--');
      }else{
        years.push((this.annoActual-index).toString());
      }
      index = index-1;
    }
    return years;
  }
  private year = this.anosAnteriores();

  private data = { };

  private getDataForWeekPeriod(tamanno:number) {
    return {
      chartLabel: this.period.getWeeks(),
      linesData: [
        [
          5, 63, 113, 45, 194, 225,
          250, 0
        ]
      ],
    };
  }

  private getDataForMonthPeriod(): OrdersChart {
    return {
      chartLabel: this.period.getMonths(),
      linesData: [
        [
          5, 63, 113, 156, 289, 225,
          250, 270, 283, 194, 290,
          286, 0
        ]
      ],
    };
  }

  private getDataForYearPeriod(): OrdersChart {
    return {
      chartLabel: this.year,
      linesData: [
        [
          190, 269, 150, 366, 200, 398,
          54,0
        ]
      ],
    };
  }

  getDataLabels(cantidadDatosLista: number, labelsArray: string[]): string[] {
    const labelsArrayLength = labelsArray.length;
    const step = Math.round(cantidadDatosLista / labelsArrayLength);
    return Array.from(Array(cantidadDatosLista)).map((item, index) => {
      const dataIndex = Math.round(index / step);
      return index % step === 0 ? labelsArray[dataIndex] : '';
    });
  }


  //se supone que acepta any. Pero en realidad solo puede aceptar integer
   async cargarDatosAPI(deviceid: any){
    let device = new HttpParams()
      .set('deviceid', deviceid)
      .set('sistema_id',this.authService.getSistemaId().toString())
    let obtenerURL = this.URL.concat('paneles/obtener');
    let query = await this.http.post<DatosPaneles[]>(obtenerURL, device,
        {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        }).toPromise();
    return query;
  }

  
  async cargarDatosPaneles(deviceid:any){
    await this.cargarDatosAPI(deviceid).
    then(async res=>{
      this.listaDatos = await res;
    })
  }

  //es una funcion auxiliar. Este crea un diccionario con los años
  //anteriores. Esto para poder cargar datos con respecto a cada año
  crearDiccionario(lista:any){
    let diccionario= {};
    lista.forEach(element=>{
      diccionario[element] = 0;
    })
    return diccionario;
  }

  crearListaDatos(period:string, dispositivo:any){
    let listaTemporal : number[] = []; 

    let diccionarioAnnos = this.crearDiccionario(this.year);
    let diccionarioMeses = this.crearDiccionario(this.period.getMonths());
    let diccionarioSemana = this.crearDiccionario(this.period.getWeeks());

    this.listaDatos.forEach(element=>{
      let tipoFecha = new Date(element.fecha);
      if(period === 'week'){

      }
      else if(period ==='month'){
        for(let i in diccionarioMeses){
          let mes = this.period.getMonths()[tipoFecha.getUTCMonth()];
          if(mes == i && tipoFecha.getFullYear()===this.annoActual){
            diccionarioMeses[i]+= element.power;
          }
        }
      }
      else{
        for (let i in diccionarioAnnos) {
          if(tipoFecha.getFullYear().toString() == i){
            diccionarioAnnos[i]+= element.power;
          }
        } 
      }
    });
    let valuesAnnos : number[] = Array.from(Object.values(diccionarioAnnos));
    let valuesMeses : number[] = Array.from(Object.values(diccionarioMeses));
    let valuesSemanas : number[] = Array.from(Object.values(diccionarioSemana));

 //   console.log(this.crearAnnosDiccionario());
    console.log(diccionarioAnnos);
    console.log(diccionarioMeses);
  //  console.log(diccionarioWeek);

    this.listaPower.length=0;
    if(period === 'week'){
      listaTemporal = valuesSemanas;
    }
    else if(period ==='month'){
      listaTemporal = valuesMeses;
    }
    else{
      listaTemporal = valuesAnnos;
    }

    this.listaPower.push(listaTemporal);
  }

  async getOrdersChartData(period: string, idDispositivo:any) {
    if(idDispositivo != '--Seleccione un dispositivo--'){
    //esto porque al inicio no se carga un dispositivo. Ya que es una llamada al servidor
    //y toma un tiempo.
      await this.cargarDatosPaneles(idDispositivo);
      await this.crearListaDatos(period,idDispositivo);

      this.data = {
        week: this.getDataForWeekPeriod(42),
        month:  {
                  chartLabel: this.period.getMonths(),
                  linesData: this.listaPower
                },
        year: {
          chartLabel: this.year,
          linesData: this.listaPower
        }
      };
    }

    else{
      this.data = {
        week: this.getDataForWeekPeriod(42),
        month: this.getDataForMonthPeriod(),
        year: this.getDataForYearPeriod(),
      };
    }
    return this.data[period];
  }
}
