import {Component, OnInit} from '@angular/core';

import {
  MENU_USUARIO,
  MENU_ADMINISTRADOR_EMPRESA,
  MENU_ADMINISTRADOR_SISTEMA,
} from './home-menu';
import {AuthService} from '../auth/auth.service';
import {NbMenuItem} from '@nebular/theme';

@Component({
  selector: 'ngx-admin',
  template:`
            <ngx-main-layout>
              <nb-menu [items]="menu"></nb-menu>
              <router-outlet></router-outlet>
            </ngx-main-layout>
          `,
})
export class MainComponent implements OnInit { 
  menu: NbMenuItem[];
  constructor(private authService: AuthService) {}
  ngOnInit(): void {
    switch (this.authService.getRol()) {
      case 1: {
        this.menu = MENU_ADMINISTRADOR_SISTEMA;
        break;
      }
      case 2: {
        this.menu =  MENU_ADMINISTRADOR_EMPRESA;
        break;
      }
      case 3: {
        this.menu =  MENU_USUARIO;
        break;
      }
      default: {
        break;
      }
    }
  }
}
