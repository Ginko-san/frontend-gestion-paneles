import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MostrarComponent} from './mostrar/mostrar.component';
import { DispositivosComponent } from './dispositivos.component';
const routes: Routes = [  {
  path: '',
  component: DispositivosComponent,
  children: [
    {
      path: 'mostrar',
      component: MostrarComponent,
    },
  ],
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DispositivosRoutingModule { }
