import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro',
  pure: false,
})
export class FiltroDispositivosPipe implements PipeTransform {
  private
  transform(inversores: any, busqueda: any, parametro: string): any {
    if ( busqueda === undefined) return inversores;
    return inversores.filter(function(inverser){
      //filtro por nombre
      if (parametro === 'Nombre'.toString()) {
        return inverser.nombre.toLowerCase().includes(busqueda.toLowerCase());
      //filtro por correo electrónico
      }else if (parametro === 'Sistema'.toString()) {
        return inverser.nombre_sistema.toLowerCase().includes(busqueda.toLowerCase());
      //filtro por empresa
      }else if (parametro === 'Número serial'.toString()) {
        return inverser.serialnum.toLowerCase().includes(busqueda.toLowerCase());
      //filtro por nombre de usuario
      }else if (parametro === 'ID dispositivo'.toString()) {
        return inverser.deviceid.toString().toLowerCase().includes(busqueda.toLowerCase());
      //filtro por rol
      }else {
        return inverser;
      }  
        })    
  }
}