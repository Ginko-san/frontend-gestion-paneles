import { Component, OnInit } from '@angular/core';
import { NgbModalOptions, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalConfirmacionService } from '../../globales/confirmacion-modal/confirmacion-modal.component.service';
import { DispositivosService } from '../dispositivos.service';
import { ToasterManagerService } from '../../../@core/toast/toaster-manager.service';
import { configToasterManager } from '../../../@core/toast/config';
import { Dispositivos } from '../dispositivos';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'ngx-mostrar',
  templateUrl: './mostrar.component.html',
  styleUrls: ['./mostrar.component.scss']
})
export class MostrarComponent implements OnInit {

  constructor(private authService: AuthService, private confirmacion: ModalConfirmacionService, private modalService: NgbModal,
    private serviciosDispositivos: DispositivosService, private toasterManagerService: ToasterManagerService) {
  }
  
  ngOnInit() {
    this.sistemaActual = this.authService.getSistemaId();
    this.cargarInversores();
  }
  
  modal : NgbModalOptions = {};
  filtros = ['Nombre', 'Sistema', 'Número serial','ID dispositivo'];

  seleccionado : any = 'Nombre';
  busqueda: string;
  response: any;
  config = configToasterManager;

  sistemaActual : number;

  private listaDispositivos : Dispositivos[] = [];
 // private  listaUsuariosAux : Dispositivos[] = [];

  cargarInversores(){
    this.serviciosDispositivos.mostrarDispositivos()
    .subscribe(
      res =>
        { 
          res.forEach(element=>{
            if(element.sistema_id===this.authService.getSistemaId()){
              this.listaDispositivos.push(element);
            }
          })
        },
      error=>{
        this.toasterManagerService.makeToast('error','Error','Error al obtener los dispositivos')
      },
    );
  }
}
