import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment.prod'
//import { Usuario, Sistemas, Roles} from './usuario';
//import {Usuario} from '././../../auth/usuario';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Dispositivos} from './dispositivos';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable()
export class DispositivosService {
  URL: string = `http://${environment.server}/`;
  httpClient: any;

  constructor(private http: HttpClient) { }

  mostrarDispositivos(): Observable<Dispositivos[]> {
    let obtenerURL = this.URL.concat('dispositivos/obtener');
    return this.http.get<Dispositivos[]>(obtenerURL, {responseType:'json'});
  }
}
