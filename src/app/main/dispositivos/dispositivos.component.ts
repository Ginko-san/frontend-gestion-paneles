import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-dispositivos',
  template: `<router-outlet></router-outlet>`,
})
export class DispositivosComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
