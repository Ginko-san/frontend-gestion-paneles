import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { InsertarComponent } from './insertar/insertar.component';
import { ThemeModule } from '../../@theme/theme.module';
import { MostrarComponent } from './mostrar/mostrar.component';
//import { NgxPaginationModule } from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';
import { ToasterManagerService } from '../../@core/toast/toaster-manager.service';
import { ToasterModule } from 'angular2-toaster';
import { MainModule } from '../../main/main.module';
import { ModalConfirmacionService } from '../globales/confirmacion-modal/confirmacion-modal.component.service';
import { ConfirmacionModalComponent} from '../globales/confirmacion-modal/confirmacion-modal.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { DispositivosRoutingModule } from './dispositivos-routing.module';
import { DispositivosComponent } from './dispositivos.component';
import { DispositivosService } from './dispositivos.service';
import { FiltroDispositivosPipe } from './filtroDispositivos.pipe';

@NgModule({
  imports: [
    CommonModule,
    DispositivosRoutingModule,
    ThemeModule,
    NgxPaginationModule,
    NgSelectModule,
    ToasterModule.forRoot(),
    MainModule,
  ],
  declarations: [DispositivosComponent, MostrarComponent, FiltroDispositivosPipe],
 //   VerificarFuncionarioDirective, VerificarUsuarioDirective],, InsertarComponent
  providers: [DispositivosService,ToasterManagerService,ModalConfirmacionService],
  entryComponents: [ConfirmacionModalComponent]  //InsertarComponent
    
    //ModalConfirmacionComponent],
})
export class DispositivosModule { }
