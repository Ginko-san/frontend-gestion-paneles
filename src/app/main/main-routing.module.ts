import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { MainComponent } from './main.component';

//import { DashboardComponent } from '../pages/dashboard/dashboard.component';
import { DashboardComponent} from './dashboard/dashboard.component';


//import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { ECommerceComponent } from '../pages/e-commerce/e-commerce.component';
//import { ECommerceComponent } from './e-commerce/e-commerce.component';
//import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

//import { } from './usuarios/usuarios.module'

const routes: Routes = [{
  path: '',
  component: MainComponent,
  children: [
    {
      path: 'dashboard',
      component: ECommerceComponent,
    },/*
    {
      path: 'predicciones',
      component: PrediccionesComponent,
    },*/
    {
      path: 'predicciones',
      loadChildren : './predicciones/predicciones.module#PrediccionesModule',
    },
    {
      path: 'usuarios',
      loadChildren : './usuarios/usuarios.module#UsuariosModule',
       // component: UsuariosComponent,
    },
    {
      path: 'sistemas',
      loadChildren : './sistemas/sistemas.module#SistemasModule',
       // component: UsuariosComponent,
    },
    {
      path: 'dispositivos',
      loadChildren : './dispositivos/dispositivos.module#DispositivosModule',
    },
    {
      path: '',
      redirectTo : 'dashboard',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {
}

