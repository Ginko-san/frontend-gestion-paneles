//se exporta la clase de usuario utilizada para el CRUD de usuarios
export class Usuario{
    id: number;
    rol_id: number;
    usuario: string;
    contrasenna: string;
    nombre: string;
    correo: string;
    sistema_id: number;
    created_at: Date;
    updated_at: Date;
    nombre_sistema: string;
    nombre_rol : string;
    success : boolean;
}

export class Roles {
    nombre: string;
    id: number;

    constructor(nombre:string, id:number ){
        this.nombre = nombre;
        this.id = id;
    }
}

export class Sistemas{
  nombre: string;
  id: number;
}

export class Mensaje {
    success: boolean;
    mensaje: string;
  }
  