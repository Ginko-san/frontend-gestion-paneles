import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuariosRoutingModule } from './usuarios-routing.module';
//import { InsertarComponent } from './insertar/insertar.component';
import { ThemeModule } from '../../@theme/theme.module';
import { MostrarComponent } from './mostrar/mostrar.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';
import { FiltroUsuarioPipe } from './filtroUsuarios.pipe';
//import { VerificarFuncionarioDirective } from './verificar-funcionario.directive';
//import { VerificarUsuarioDirective } from './verificar-usuario.directive';
//import { ModalConfirmacionComponent } from '../componentes-globales/modal-confirmacion/modal-confirmacion.component';
//import { ModalConfirmacionService } from '../componentes-globales/modal-confirmacion/modal-confirmacion.service';
import { ToasterManagerService } from '../../@core/toast/toaster-manager.service';
import { ToasterModule } from 'angular2-toaster';
import { MainModule } from '../../main/main.module';
import { UsuariosComponent } from './usuarios.component';
import { UsuariosService } from './usuarios.service'
import { ModalConfirmacionService } from '../globales/confirmacion-modal/confirmacion-modal.component.service';
import { ConfirmacionModalComponent} from '../globales/confirmacion-modal/confirmacion-modal.component';
import { ModalEditarComponent } from './modal-editar/modal-editar.component';

@NgModule({
  imports: [
    CommonModule,
    UsuariosRoutingModule,
    ThemeModule,
    NgxPaginationModule,
    NgSelectModule,
    ToasterModule.forRoot(),
    MainModule,
  ],
  declarations: [UsuariosComponent, MostrarComponent, FiltroUsuarioPipe, ModalEditarComponent],
 //   VerificarFuncionarioDirective, VerificarUsuarioDirective],, InsertarComponent
  providers: [UsuariosService,ToasterManagerService,ModalConfirmacionService],
  entryComponents: [ConfirmacionModalComponent]  //InsertarComponent
    
    //ModalConfirmacionComponent],
})
export class UsuariosModule { }
