import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { Usuario, Roles, Sistemas} from '../usuario';
import { UsuariosService } from '../usuarios.service';
//import { ModalConfirmacionService } from '../../componentes-globales/modal-confirmacion/modal-confirmacion.service';
import { ToasterManagerService } from '../../../@core/toast/toaster-manager.service';
import { configToasterManager } from '../../../@core/toast/config';
import 'style-loader!angular2-toaster/toaster.css';
import { ModalConfirmacionService } from '../../globales/confirmacion-modal/confirmacion-modal.component.service';
import { MostrarComponent } from '../mostrar/mostrar.component';
import { Router } from '@angular/router';
import { AuthService } from '../../../auth/auth.service';
@Component({
  selector: 'ngx-modal-editar',
  templateUrl: './modal-editar.component.html',
  styleUrls: ['./modal-editar.component.scss'],
})
export class ModalEditarComponent implements OnInit {
  constructor(private router :Router, private authService: AuthService,
     private modalConfirmar: ModalConfirmacionService, public activeModal: NgbActiveModal, private servicioUsuarios: UsuariosService, private toasterManagerService: ToasterManagerService) { }

    ngOnInit() {
      
      if(this.modo== "agregar"){
        this.menu = "Agregar usuario";
      }
      else{
        //cuando es editar tiene que cargar los datos en los input..
        this.cargaDatosMostrar();
        this.menu = "Editar usuario";
      }

      //se crea un nuevo usuario temporal
      //es una copia del usuario que viene por parametro del otro compnente
      this.nuevoUsuario = new Usuario();

      //este funciona como un nuevo usuario auxiliar. 
      //esto es para que no se vaya modificando el dato y posiblemente no se edite.
      this.nuevoUsuario = Object.assign({}, this.nuevoUsuario, this.usuarioActual)
      this.rolActual = this.authService.getRol();

      this.cargarOpcionesRoles();
    }
//instancia para el configurToaster
  config = configToasterManager;
  
  // variables que vienen desde el componente de consultar.
  @Input() listaUsuarios;

  //obtiene el dato que viene desde el otro componente de Mostrar
  @Input() usuarioActual : Usuario;

  //muestra la lista de sistemas que viene desde el componente anterior
  //esto para que a la hora de editar, ya esté cargado desde el inicio.
  @Input() listaSistemas;

  @Input() modo;

  selecionadoSistema : Sistemas;
  seleccionadoRol: Roles;

  rolActual: number;
//este es el que se va a enviar para modificar
  nuevoUsuario;
  //este es el tipo de menu. Ya sea editar o agregar
  menu : any;


//dependiendo del rol que tenga, este va a cargar alguno de las dos opciones que tiene.
  rolesGenerales : Roles[];
 //roles que verá solo el administrador de la página
  rolesAdministradorPagina: Roles[] = [new Roles('Administrador general página web',1), 
                                      new Roles('Administrador institución',2), 
                                      new Roles('Usuario',3)]
  rolesAdministradorInstitucion : Roles[] = [new Roles('Administrador institución',2), 
                                              new Roles('Usuario',3)]
                    

  cargarOpcionesRoles(){
    if(this.rolActual==1){
      this.rolesGenerales = this.rolesAdministradorPagina;
    }
    else{
      this.rolesGenerales = this.rolesAdministradorInstitucion;
      //esto es para darle un rol predeterminado
    }
  }
  
  cargaDatosMostrar(){
    this.listaSistemas.forEach(sistema => {
      if (sistema.nombre.toString() === this.usuarioActual.nombre_sistema) {
        this.selecionadoSistema = sistema;
      }
    });
    this.rolesAdministradorPagina.forEach(rol=>{
      if(rol.nombre.toString()=== this.usuarioActual.nombre_rol){
        this.seleccionadoRol = rol;
      }
    })
  }

  decisionModoInsertarEditar(){
    if(this.modo=="editar"){
      this.editarDatos();
    }
    else{
      this.guardarDatos();
    }
  }

guardarDatos(){
  //valida que seleccione un rol y un sistema. Solo si es administrador
    if((typeof this.seleccionadoRol === "undefined" || typeof this.selecionadoSistema === "undefined") && this.rolActual == 1){
      this.toasterManagerService.makeToast('error','Error','Debes selecionar un rol y un sistema');
    }
    else{
      //si el rol actual es administrador de pagina queda igual.
      //en caso contrario, la información de sistema es el mismo con el que se inició en el auth.
      if(this.rolActual==1)
      {
        this.nuevoUsuario.sistema_id = this.selecionadoSistema.id;
        this.nuevoUsuario.nombre_sistema = this.selecionadoSistema.nombre;
      }
      else{
        this.nuevoUsuario.sistema_id = this.authService.getSistemaId();
        this.nuevoUsuario.nombre_sistema = this.authService.getNombreSistema();
      }
      this.nuevoUsuario.rol_id = this.seleccionadoRol.id;
      this.nuevoUsuario.nombre_rol = this.seleccionadoRol.nombre;

      this.servicioUsuarios.insertarUsuario(this.nuevoUsuario).subscribe(
        usuario=>{
          //este success lo envia desde el backend. Para comprobar si ya existe usuario.
          if(usuario.success==true){
            this.nuevoUsuario.id = usuario.id;
            this.listaUsuarios.push(this.nuevoUsuario);
            //para ordenarlo después de insertarlo.
            this.toasterManagerService.makeToast('success','Éxito','Se ha insertado con éxito');
            this.cerrarModal();
          }
          else{
            this.toasterManagerService.makeToast('error','Error','Ese nombre de usuario ya existe');
          }
        },
        error=>{
          this.toasterManagerService.makeToast('error','No se pudo insertar','No se ha insertado debido a un error en el servidor');
        }
      )
    }
  }


  editarDatos(){  
    this.modalConfirmar.confirmar('Confirmar','¿Quieres editar los cambios?')
      .then((confirmed)=>{
        if(confirmed){
          this.nuevoUsuario.sistema_id = this.selecionadoSistema.id;
          this.nuevoUsuario.nombre_sistema = this.selecionadoSistema.nombre;
          this.nuevoUsuario.rol_id = this.seleccionadoRol.id;
          this.nuevoUsuario.nombre_rol = this.seleccionadoRol.nombre;
          this.servicioUsuarios.modificarUsuario(this.nuevoUsuario).subscribe(
            usuario=>{
              if(usuario.success==false){
                this.toasterManagerService.makeToast('error','No se pudo modificar','Ya existe un usuario con ese usuario');
              }
              else{
                this.toasterManagerService.makeToast('success','Éxito','Se ha modificado con éxito');
                this.usuarioActual.sistema_id = this.selecionadoSistema.id;
                this.usuarioActual.nombre_sistema = this.selecionadoSistema.nombre;
                this.usuarioActual.rol_id = this.seleccionadoRol.id;
                this.usuarioActual.nombre_rol = this.seleccionadoRol.nombre;
                this.usuarioActual.correo = this.nuevoUsuario.correo;
                this.usuarioActual.nombre = this.nuevoUsuario.nombre;
                this.usuarioActual.usuario = this.nuevoUsuario.usuario;
                this.cerrarModal();
              }
            },
            error=>{
              this.toasterManagerService.makeToast('error','No se pudo modificar','No se ha modificado debido a un error en el servidor');
              this.cerrarModal();
            }
          )
        }
      })
  }


  cerrarModal() {
    this.activeModal.close()
  }





  /* observer de funcionarios, para que cuando se obtengan los funcionarios, si es modificar,
  se busca el funcionario al que corresponde el usuario a modificar*/
 /* observerFuncionarios = {
    // primero se obtienen los datos de funcionarios
    next: datosFuncionarios => { this.Funcionarios = datosFuncionarios },
    // en caso de error
    error: err => error => {
      this.toasterManagerService.makeToast('error', 'No se puede obtener funcionarios! ',
        'No se puede obtener funcionarios debido a un error con el servidor.');
    },
    /* cuando se obtengan los funcionarios para el select, si es modificar, se selecciona el funcionario,
     al que pertenece el usuario en el select
    complete: () => {
      if (this.usuario !== null) {
        this.Funcionarios.forEach(fun => {
          if (fun.cedula === this.usuario.cedula) {
            this.funcionario = fun;
          }
        });
      }
    },
};

  // titulo de la ventana, si es modificar o insertar
  private titulo;


  // Al abrir el modal
  ngOnInit() {
    this.solicitudActual = new Usuario();
    // se carga la lista de funcionarios para el modal
    this.usuariosService.consultarFuncionarios()
      .subscribe(this.observerFuncionarios);
    // si el usuario que viene desde consultar es null, es insertar en caso contrario es modificar.
    if (this.usuario == null) {
      this.titulo = 'Insertar';
      this.checkboxStepi = false;
      this.checkboxSer = false;
    } else {
      this.titulo = 'Modificar';
      // deshabilitar el select
      this.deshabilitado = true;
      // se copia el usuario que viene desde consultar a la solicitud actual
      this.solicitudActual = Object.assign({}, this.solicitudActual, this.usuario);
      if (this.solicitudActual != null)
        console.log(this.solicitudActual.SER);
      if (this.solicitudActual.STEPI === '0' || this.solicitudActual.STEPI === null) {
        this.checkboxStepi = false;
        this.solicitudActual.STEPI = null;
      } else {
        this.checkboxStepi = true;
      }
      if (this.solicitudActual.SER === '0' || this.solicitudActual.SER === null) {
        this.checkboxSer = false;
        this.solicitudActual.SER = null;
      } else {
        this.checkboxSer = true;
      }
      this.opcionesStepi.forEach(stepi => {
        if (stepi.acceso.toString() === this.solicitudActual.STEPI) {

          this.accesoStepi = stepi;
        }
      });
      this.opcionesSer.forEach(ser => {
        if (ser.acceso.toString() === this.solicitudActual.SER) {

          this.accesoSer = ser;
        }
      });
    }
  }
  // funcion de busqueda de los funcionarios en el select
  busquedaFuncionarios(texto: string, item: Funcionario) {
    texto = texto.toLocaleLowerCase();
    return item.nombre.toLocaleLowerCase().includes(texto) || item.cedula.toString().toLocaleLowerCase().includes(texto);
  }
  limpiarSelectStepi(checkbox: boolean) {
    if (!checkbox) {
      this.accesoStepi = null;
    }
  }
  limpiarSelectSer(checkbox: boolean) {
    if (!checkbox) {
      this.accesoSer = null;
    }
  }
  cerrar() {
    this.activeModal.close()
  }
  
  // para guardar los cambios
  guardarDatos() {
    // cuando es insertar
    if (this.usuario == null) {
      // se asigna la cedula del funcionario a la cedula del usuario
      this.solicitudActual.cedula = this.funcionario.cedula;
      if (this.accesoStepi !== null && this.accesoStepi !== undefined) {
        this.solicitudActual.STEPI = this.accesoStepi.acceso;
      } else {
        this.solicitudActual.STEPI = null;
      }
      if (this.accesoSer !== null && this.accesoSer !== undefined) {
        this.solicitudActual.SER = this.accesoSer.acceso;
      } else {
        this.solicitudActual.SER = null;
      }
      // se envia post al backend para insertar usuarios.
      this.usuariosService.insertarUsuario(this.solicitudActual).subscribe(
        /* se recibe el usuario que se inserto a la base de datos, y se agrega a la lista para poder
        visuarlizar cambios*//*
        usuario => {
          this.listaUsuarios.push(usuario[0]);
          this.toasterManagerService.makeToast('success', 'Se ha insertado exitosamente!',
            'Se ha insertado el usuario ' + this.solicitudActual.nombre_usuario +
            ' correctamente.');
        },
        error => {
          this.toasterManagerService.makeToast('error', 'No se completo el agregar! ',
            'No se ha agregado el usuario ' + this.solicitudActual.nombre_usuario +
            ' debido a un error con el servidor.');
        },
      );
      this.activeModal.close()
    } else {
      if (this.accesoStepi !== null && this.accesoStepi !== undefined) {
        this.solicitudActual.STEPI = this.accesoStepi.acceso;
      } else {
        this.solicitudActual.STEPI = null;
      }
      if (this.accesoSer !== null && this.accesoSer !== undefined) {
        this.solicitudActual.SER = this.accesoSer.acceso;
      } else {
        this.solicitudActual.SER = null;
      }


      this.modalConfirmacionService.confirmar('Por favor confirme..', '¿Quiere modificar el usuario ' + this.usuario.nombre_usuario + '?')
        .then((confirmed) => {
          if (confirmed) {
            // se envia put al backend para modificar usuarios.
            this.usuariosService.modificarUsuario(this.solicitudActual).subscribe(
              /* se recibe el usuario que se modifico en la base de datos, y se modifica en la lista para poder
              visuarlizar cambios*//*
              usuario => {
                this.usuario.nombre_usuario = usuario[0].nombre_usuario;
                this.usuario.STEPI = usuario[0].STEPI;
                this.usuario.SER = usuario[0].SER;
                this.toasterManagerService.makeToast('success', 'Se ha modificado exitosamente!',
                  'Se ha modificado el usuario ' + this.solicitudActual.nombre_usuario +
                  ' correctamente.');
              },
              error => {
                this.toasterManagerService.makeToast('error', 'No se completo el modificar! ',
                  'No se ha modificado el usuario ' + this.solicitudActual.nombre_usuario +
                  ' debido a un error con el servidor.');
              },
            );
            // se cierra la ventana del modal
            this.activeModal.close()
          }
        })
    }
  }*/
}
