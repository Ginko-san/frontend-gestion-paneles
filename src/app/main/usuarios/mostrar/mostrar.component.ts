import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalOptions, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToasterManagerService } from '../../../@core/toast/toaster-manager.service';
import { UsuariosService } from '../usuarios.service';
//este es el usuario que es solo para la gestión
import { Usuario, Sistemas, Roles } from '../usuario';
import { configToasterManager } from '../../../@core/toast/config';

import {ModalConfirmacionService} from '../../globales/confirmacion-modal/confirmacion-modal.component.service';
import { ModalEditarComponent } from '../modal-editar/modal-editar.component';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'ngx-mostrar',
  templateUrl: './mostrar.component.html',
  styleUrls: ['./mostrar.component.scss']
})
export class MostrarComponent implements OnInit {
  //falta private modalConfirmacionService: ModalConfirmacionService,
  constructor( private confirmacion: ModalConfirmacionService, private modalService: NgbModal,
    private authService: AuthService,
    private servicioUsuarios: UsuariosService,
     private toasterManagerService: ToasterManagerService) {
  }

  modal : NgbModalOptions = {};
  filtros = ['Nombre', 'Correo', 'Empresa','Usuario','Rol'];

  administradorPagina : any = 'Administrador página web';

  seleccionado : any = 'Nombre';
  busqueda: string;
  response: any;
  config = configToasterManager;

  private listaSistemas : Sistemas[];
  private listaRoles : Roles[];
  private listaUsuarios : Usuario[];

  obtenerUsuarios():void{
    this.servicioUsuarios.mostrarUsuarios().subscribe
      (res => 
        {
          //el rol 1 es el administrador pagina. Tiene acceso a todo
          //en caso de que no sea administrador general, solo mostrará usuarios
          //que sean del mismo sistema
          var rolUsuarioActual = this.authService.getRol();
          var sistemaUsuarioActual = this.authService.getSistemaId();
          this.listaUsuarios = res;

          //para tener una lista auxiliar de los datos que vienen en el response,
          //se usa el response completo cuando es admnistrador general.
          var listaUsuariosAux : Usuario[] = [];

          if(rolUsuarioActual!=1){
            this.listaUsuarios.forEach((element,index)=>{
              if(element.rol_id!=1 && element.sistema_id==sistemaUsuarioActual)
                listaUsuariosAux.push(element);
            })
            this.listaUsuarios = listaUsuariosAux;
          }
        },
      error=>{
        this.toasterManagerService.makeToast('error','Error','Error al obtener los usuarios')
      },
    );
  }

  mostrarSistemas():void{
    this.servicioUsuarios.mostrarSistemas()
      .subscribe(res => {this.listaSistemas = res},
        error=>{
          this.toasterManagerService.makeToast('error','Error','Error al obtener los sistemas')
        },
      );
  }

  mostrarRoles():void{
    this.servicioUsuarios.mostrarRoles()
      .subscribe(res=>{this.listaRoles = res},
        error=>{
          this.toasterManagerService.makeToast('error','Error','Error al obtener los roles.')
        });
  }


  public ngOnInit() {
    this.obtenerUsuarios();

    //estos se envian al otro componente
    this.mostrarSistemas();
    this.mostrarRoles();     
  }



  eliminar(usuario: Usuario){
    this.confirmacion.confirmar('Confirmación','¿Está seguro que desea eliminar este usuario?')
      .then((confirmado)=>{
        if(confirmado){
          const posicion = this.listaUsuarios.findIndex((user:Usuario)=>
          {
            return user.id === usuario.id;
          },
          );
          this.servicioUsuarios.eliminarUsuario(usuario.id).subscribe(
            (data)=>{
              this.listaUsuarios.splice(posicion,1),
              this.toasterManagerService.makeToast('success','Éxito','Se ha eliminado con éxito')
            },
          (error) =>{
            this.toasterManagerService.makeToast('error',' Error','Hubo un problema al eliminar el usuario')
          }
          );
          }
        })
    }

  crearModal(usuarioData: Usuario){
    this.modal.backdrop = 'static';
    this.modal.keyboard = false;
    const ref = this.modalService.open(ModalEditarComponent)
    ref.componentInstance.listaUsuarios = this.listaUsuarios;

    //esto es para verificar si es editar o modificar..
    if(usuarioData ==null){
      ref.componentInstance.modo = "agregar";
    }
    else{
      ref.componentInstance.modo = "editar";
    }
    ref.componentInstance.usuarioActual = usuarioData;
    ref.componentInstance.listaSistemas = this.listaSistemas;
    ref.componentInstance.listaRoles = this.listaRoles;
  }

  }
  