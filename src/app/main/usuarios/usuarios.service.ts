import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment.prod'
import { Usuario, Sistemas, Roles} from './usuario';
//import {Usuario} from '././../../auth/usuario';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

@Injectable()
export class UsuariosService {
  URL: string = `http://${environment.server}/`;

  constructor(private http: HttpClient) { }

  mostrarUsuarios(): Observable<Usuario[]> {
    let obtenerURL = this.URL.concat('usuarios/obtener');
    return this.http.get<Usuario[]>(obtenerURL, {responseType:'json'});
  }

  insertarUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.URL.concat('usuarios/insertar'),  usuario, { responseType: 'json' })
  }
  modificarUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.put<Usuario>(this.URL.concat('usuarios/modificar/'+ usuario.id), usuario, { responseType: 'json' })
  }
  eliminarUsuario( id: number):Observable<{}>{
    return this.http.delete(this.URL.concat('usuarios/eliminar/' + id), { responseType: 'text' });
  }

  mostrarSistemas(): Observable<Sistemas[]>{
    return this.http.get<Sistemas[]>(this.URL.concat('sistemas/obtener'), {responseType:'json'});
  }

  mostrarRoles(): Observable<Roles[]>{
    return this.http.get<Roles[]>(this.URL.concat('roles/obtener'), {responseType:'json'});
  }

}
