import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro',
  pure: false,
})
export class FiltroUsuarioPipe implements PipeTransform {
  private
  transform(usuarios: any, busqueda: any, parametro: string): any {
    if ( busqueda === undefined) return usuarios;
    return usuarios.filter(function(usuario){
      //filtro por nombre
      if (parametro === 'Nombre'.toString()) {
        return usuario.nombre.toLowerCase().includes(busqueda.toLowerCase());
      //filtro por correo electrónico
      }else if (parametro === 'Correo'.toString()) {
        return usuario.correo.toLowerCase().includes(busqueda.toLowerCase());
      //filtro por empresa
      }else if (parametro === 'Empresa'.toString()) {
        return usuario.nombre_sistema.toLowerCase().includes(busqueda.toLowerCase());
      //filtro por nombre de usuario
      }else if (parametro === 'Usuario'.toString()) {
        return usuario.usuario.toLowerCase().includes(busqueda.toLowerCase());
      //filtro por rol
      }else if (parametro === 'Rol'.toString()) {
        return usuario.nombre_rol.toLowerCase().includes(busqueda.toLowerCase());
      }else {
        return usuario;
      }
        })
  }
}