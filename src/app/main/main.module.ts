import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { ThemeModule} from '../@theme/theme.module';
//import {DashboardModule } from './dashboard/dashboard.module';
//import { ECommerceModule } from './e-commerce/e-commerce.module';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuariosModule } from './usuarios/usuarios.module';
import { ConfirmacionModalComponent } from './globales/confirmacion-modal/confirmacion-modal.component';


//para cuando quiero probar con el Dashboard General
//import {DashboardModule} from '../pages/dashboard/dashboard.module';
import {ECommerceModule} from '../pages/e-commerce/e-commerce.module';

const MAIN_COMPONENTS = [
  MainComponent,
];
@NgModule({
  imports: [
 //   DashboardModule,
    CommonModule,
    MainRoutingModule,
    ThemeModule,
    ECommerceModule
  ],
  declarations: [...MAIN_COMPONENTS,ConfirmacionModalComponent]
})
export class MainModule { }
