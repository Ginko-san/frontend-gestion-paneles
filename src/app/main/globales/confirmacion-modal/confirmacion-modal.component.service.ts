import { Injectable } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmacionModalComponent } from '../confirmacion-modal/confirmacion-modal.component';

@Injectable()
export class ModalConfirmacionService {

  //el constructor manda el NgbModal de boostrap
  constructor(private servicioModal: NgbModal) { }
  
  //esta función se encarga de crear las caracteristicas del modal a crear..
  public confirmar(
    tituloModal: string,
    mensajeModal: string,
    botonOk: string = 'Aceptar',
    botonCancelar: string = 'Cancelar',
    tamannoDialog: 'sm' | 'lg' = 'sm'): Promise<boolean> {
    var modalOption: NgbModalOptions = {};
    modalOption.size = tamannoDialog;
    const modalReferefencia = this.servicioModal.open(ConfirmacionModalComponent, modalOption);
    modalReferefencia.componentInstance.tituloModal = tituloModal;
    modalReferefencia.componentInstance.mensajeModal = mensajeModal;
    modalReferefencia.componentInstance.botonOk = botonOk;
    modalReferefencia.componentInstance.botonCancelar = botonCancelar;
    return modalReferefencia.result;
  }
}
