import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit , Input} from '@angular/core';

@Component({
  selector: 'ngx-confirmacion-modal',
  templateUrl: './confirmacion-modal.component.html',
  styleUrls: ['./confirmacion-modal.component.scss']
})
export class ConfirmacionModalComponent implements OnInit {

  @Input() tituloModal: string;
  @Input() mensajeModal: string;
  @Input() botonOk: string;
  @Input() botonCancelar: string;
  constructor(public modalActivo: NgbActiveModal) { }

  ngOnInit() {
  }
  public aceptar() {
    this.modalActivo.close(true);
  }
  public cancelar() {
    this.modalActivo.close(false);
  }
  public dismiss() {
    this.modalActivo.dismiss();
  }
}
