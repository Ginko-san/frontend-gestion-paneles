import { Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { getCurrentDebugContext } from '@angular/core/src/view/services';

@Component({
  selector: 'ngx-temperatura',
  styleUrls: ['./temperatura.component.scss'],
  templateUrl: './temperatura.component.html',
})
export class TemperaturaComponent implements OnDestroy {

  temperature = document.querySelector(".weather");
  temperatureOff = false;
  temperatureMode = 'cool';

  humidity = 87;
  humidityOff = false;
  humidityMode = 'heat';

  colors: any;
  themeSubscription: any;

  constructor(private theme: NbThemeService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      this.colors = config.variables;
    });
  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
  }
}
