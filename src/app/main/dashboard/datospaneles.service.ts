import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment.prod';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class UsuariosService {
  URL: string = `http://${environment.server}/`;

  constructor(private http: HttpClient) { }

  obtenerDatosPaneles( id: number):Observable<{}>{
    return this.http.get(this.URL.concat('paneles/obtener/' + id), { responseType: 'text' });
  }


}
