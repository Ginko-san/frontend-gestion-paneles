import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { TemperaturaComponent } from './temperatura/temperatura.component';
import { TrafficComponent } from '../../pages/dashboard/traffic/traffic.component';
import { TrafficChartComponent } from '../../pages/dashboard/traffic/traffic-chart.component';
import { TemperatureDraggerComponent } from '../../pages/dashboard/temperature/temperature-dragger/temperature-dragger.component';
import { WeatherComponent } from './weather/weather.component';
import { SolarComponent } from './solar/solar.component';
import { ElectricityComponent } from '../../pages/dashboard/electricity/electricity.component';
import { ElectricityChartComponent } from '../../pages/dashboard/electricity/electricity-chart/electricity-chart.component';
import { TemperatureComponent } from './temperature/temperature.component';
import { OrdersChartComponent } from './charts-panel/charts/orders-chart.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgxEchartsModule } from 'ngx-echarts';
import { ChartModule } from 'angular2-chartjs';
import { ECommerceChartsPanelComponent } from './charts-panel/charts-panel.component';

@NgModule({
  imports: [
    ThemeModule,
    ChartModule,
    NgxEchartsModule,
    NgxChartsModule,
  ],
  declarations: [
    //este es el general
    DashboardComponent,
   // ECommerceChartsPanelComponent,
  //  TemperaturaComponent,
  //  DashboardComponent,
   // StatusCardComponent,
//     TemperatureDraggerComponent,
  //  ContactsComponent,
  //  RoomSelectorComponent,
 //    TemperatureComponent,
  //  RoomsComponent,
  //  TeamComponent,
   // KittenComponent,
  //  SecurityCamerasComponent,
 //     ElectricityComponent,
  //    ElectricityChartComponent,
 //     WeatherComponent,
  //  PlayerComponent,
  //    SolarComponent,
  //  TrafficComponent,
   // TrafficChartComponent,
  ],
})
export class DashboardModule { }
