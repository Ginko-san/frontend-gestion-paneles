export class Predicciones{
    upr: number;
    lwr: number;
    fit: number;
    _row: string;
}

export class Intervalos {
    id: number;
    name: string;
    
    constructor(id:number, name:string){
        this.id = id;
        this.name = name;
    }
}

export class ResponseT {
    summary: string;
    status: string;
}