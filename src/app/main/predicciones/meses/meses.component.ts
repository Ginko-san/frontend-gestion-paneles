import { Component, OnInit } from '@angular/core';

import { ToasterManagerService } from '../../../@core/toast/toaster-manager.service';
import { configToasterManager } from '../../../@core/toast/config';
import { AuthService } from '../../../auth/auth.service';

import { PrediccionesService } from '../predicciones.service';
import { Predicciones, Intervalos } from "../predicciones";

@Component({
  selector: 'ngx-meses',
  templateUrl: './meses.component.html',
  styleUrls: ['./meses.component.scss']
})
export class MesesComponent implements OnInit {
  zoomingData: Predicciones[];

  showSpinner: boolean = false;

  constructor( 
    private prediccionesService: PrediccionesService,
    private toasterManagerService: ToasterManagerService,
    private authService: AuthService ) { 
  }

  customizeTooltip = (info: any) => {
    return {
      html: "<div><div class='tooltip-header'>" +
        info.argumentText + "</div>" +
        "<div class='tooltip-body'><div class='series-name'><strong>" +
        info.points[0].seriesName +
        "</strong>: </div><div class='value-text'>" +
        info.points[0].valueText +
        "</div><div class='series-name'><strong>" +
        info.points[2].seriesName +
        "</strong>: </div><div class='value-text'>" +
        info.points[2].valueText +
        "</div><div class='series-name'><strong>" +
        info.points[1].seriesName +
        "</strong>: </div><div class='value-text'>" +
        info.points[1].valueText +
        "</div></div></div>"
    };
  }

  config = configToasterManager;
  rolActual: number;

  predictionsList : Predicciones[] = [
    { upr: 2.0, lwr: 0, fit: 1.0, _row: new Date(2019, 5, 1).toDateString() },
    { upr: 2.0, lwr: 0, fit: 1.0, _row: new Date(2019, 6, 1).toDateString() },
    { upr: 2.0, lwr: 0, fit: 1.0, _row: new Date(2019, 7, 1).toDateString() },
  ];

  obtenerPredicciones(meses: number, interval: number):void{
    this.showSpinner = true;
    this.prediccionesService.prediccionMeses(meses, interval)
      .subscribe(res => {this.predictionsList = res; this.showSpinner = false;},
        error => {
          this.toasterManagerService.makeToast('error', 'Error', 'Error al obtener las predicciones')
        },
      );
  }

  ngOnInit() {
    this.cargarDatosMostrar();

    this.rolActual = this.authService.getRol();
    // Por default realiza la consulta a seis meses,
    //  con intervalo de 0.80

    this.obtenerPredicciones(6, 0);
  }

  seleccionadoIntConf : Intervalos;
  mesesAPredecir : number;
  
  intervalosConfianza : Intervalos[] = [new Intervalos(0, '80%'),
                                        new Intervalos(1, '90%'),
                                        new Intervalos(2, '95%'),
                                        new Intervalos(3, '99%')];

  cargarDatosMostrar(){
    this.intervalosConfianza.forEach(intervalo => this.seleccionadoIntConf = intervalo);
  }

  solicitarPrediccion() {
    this.showSpinner = true;
    if (typeof this.seleccionadoIntConf === "undefined" || typeof this.mesesAPredecir === "undefined") {
      this.toasterManagerService.makeToast('error','Error','Debes selecionar un intervalo y una cantidad de meses a predecir');
    } else {
      this.prediccionesService.prediccionMeses(this.mesesAPredecir, this.seleccionadoIntConf.id).subscribe(
        prediccion => {
          this.predictionsList = prediccion;
          this.toasterManagerService.makeToast('success','Éxito','Se ha predicho con éxito');
          this.showSpinner = false;
        }, 
        error => {
          this.toasterManagerService.makeToast('error','No se pudo predecir','No se ha predecido debido a un error en el servidor');
          this.showSpinner = false;
        }
      )
    }
  }

  reEntrenarButton() {
    this.showSpinner = true;

    this.prediccionesService.reEntrenarPatron().subscribe(
      response => {
        console.log(response)
        if (response[0].status === 200) {
          this.toasterManagerService.makeToast('success','Éxito','Se ha re-entrenado el patron con éxito');
        } else {
          this.toasterManagerService.makeToast('error','No se pudo re-entrenar',`${response[0].summary}`);  
        }
        this.showSpinner = false;
      },
      error => {
        this.toasterManagerService.makeToast('error','No se pudo re-entrenar','No se ha entrenado el patron debido a un error en el servidor');
        this.showSpinner = false;
      }
    )
  }

}
