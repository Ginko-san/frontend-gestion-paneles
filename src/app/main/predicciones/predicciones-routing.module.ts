import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrediccionesComponent } from './predicciones.component';
import { MesesComponent } from './meses/meses.component';

const routes: Routes = [  {
  path: '',
  component:  PrediccionesComponent,
  children: [
    {
      path: 'meses',
      component: MesesComponent
    },
    // En caso de mas predicciones agreagar rutas aqui...
  ],
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrediccionesRoutingModule { }
