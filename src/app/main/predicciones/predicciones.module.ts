import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThemeModule } from '../../@theme/theme.module';
import { ToasterManagerService } from '../../@core/toast/toaster-manager.service';
import { ToasterModule } from 'angular2-toaster';
import { MainModule } from '../../main/main.module';
import { NgSelectModule } from '@ng-select/ng-select';

import { PrediccionesRoutingModule } from './predicciones-routing.module';
import { PrediccionesComponent } from './predicciones.component';
import { PrediccionesService } from './predicciones.service';
import { MesesComponent } from './meses/meses.component';

import { LoadingSpinnerComponent } from '../ui/loading-spinner/loading-spinner.component'

import { DxChartModule } from 'devextreme-angular';

@NgModule({
  imports: [
    CommonModule,
    PrediccionesRoutingModule,
    ThemeModule,
    NgSelectModule,
    ToasterModule.forRoot(),
    MainModule,
    DxChartModule
  ],
  declarations: [PrediccionesComponent, MesesComponent, LoadingSpinnerComponent],
  providers:  [PrediccionesService, ToasterManagerService]
})
export class PrediccionesModule { }