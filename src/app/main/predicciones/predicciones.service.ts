import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment.prod';
import { Predicciones, ResponseT } from './predicciones';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};
/*
const zoomingData: Predicciones[] = [
  { upr: 727.8931, lwr: 388.9154, fit: 558.4043, _row: new Date(2019, 5, 1).toDateString() },
  { upr: 740.394, lwr: 380.9643, fit: 560.6791, _row: new Date(2019, 6, 1).toDateString() },
  { upr: 753.1604, lwr: 372.7476, fit: 562.954, _row: new Date(2019, 7, 1).toDateString() },
  { upr: 766.1827, lwr: 364.2751, fit: 565.2289, _row: new Date(2019, 8, 1).toDateString() },
  { upr: 779.4525, lwr: 355.555, fit: 567.5037, _row: new Date(2019, 9, 1).toDateString() },
  { upr: 792.9621, lwr: 346.5951, fit: 569.7786, _row: new Date(2019, 10, 1).toDateString() },
  { upr: 806.7046, lwr: 337.4024, fit: 572.0535, _row: new Date(2019, 11, 1).toDateString() },
  { upr: 820.6736, lwr: 327.9831, fit: 574.3284, _row: new Date(2019, 12, 1).toDateString() },
  { upr: 834.8634, lwr: 318.3431, fit: 576.6032, _row: new Date(2020, 1, 1).toDateString() },
  { upr: 849.2686, lwr: 308.4876, fit: 578.8781, _row: new Date(2020, 2, 1).toDateString() },
  { upr: 863.8844, lwr: 298.4216, fit: 581.153, _row: new Date(2020, 3, 1).toDateString() },
  { upr: 878.7061, lwr: 288.1496, fit: 583.4279, _row: new Date(2020, 4, 1).toDateString() },
  { upr: 893.7296, lwr: 277.6759, fit: 585.7027, _row: new Date(2020, 5, 1).toDateString() },
  { upr: 908.9509, lwr: 267.0043, fit: 587.9776, _row: new Date(2020, 6, 1).toDateString() },
  { upr: 924.3664, lwr: 256.1386, fit: 590.2525, _row: new Date(2020, 7, 1).toDateString() },
  { upr: 939.9725, lwr: 245.0822, fit: 592.5274, _row: new Date(2020, 8, 1).toDateString() },
  { upr: 955.7661, lwr: 233.8384, fit: 594.8022, _row: new Date(2020, 9, 1).toDateString() },
  { upr: 971.744, lwr: 222.4102, fit: 597.0771, _row: new Date(2020, 10, 1).toDateString() },
  { upr: 987.9034, lwr: 210.8005, fit: 599.352, _row: new Date(2020, 11, 1).toDateString() },
  { upr: 1004.2416, lwr: 199.0121, fit: 601.6268, _row: new Date(2020, 12, 1).toDateString() },
  { upr: 1020.7558, lwr: 187.0476, fit: 603.9017, _row: new Date(2021, 1, 1).toDateString() },
  { upr: 1037.4437, lwr: 174.9095, fit: 606.1766, _row: new Date(2021, 2, 1).toDateString() },
  { upr: 1054.3028, lwr: 162.6001, fit: 608.4515, _row: new Date(2021, 3, 1).toDateString() },
  { upr: 1071.331, lwr: 150.1217, fit: 610.7263, _row: new Date(2021, 4, 1).toDateString() } 
]*/


@Injectable()
export class PrediccionesService {
  URL: string = `http://${environment.rServer}/`;

  constructor(private http: HttpClient) { }

  prediccionMeses(meses: number, interval: number): Observable<Predicciones[]> {
    return this.http.get<Predicciones[]>(
      this.URL.concat(`predict/${meses}/with/${interval}`), 
      {
        responseType: 'json'
      }
    );
  };

  reEntrenarPatron(): Observable<ResponseT> {
    return this.http.get<ResponseT>(
      this.URL.concat('trainPredictModel'),
      { responseType: 'json' }
    );
  }
  
  // Cualquier otro modelo predictivo, la s consulta al RESTAPI se agregan
  // aqui...
}
