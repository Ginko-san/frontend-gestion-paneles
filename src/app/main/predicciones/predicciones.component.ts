import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-predicciones',
  template: `<router-outlet></router-outlet>`,
})
export class PrediccionesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
