import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro',
  pure: false,
})
export class FiltroSistemasPipe implements PipeTransform {
  private
  transform(sistemas: any, busqueda: any, parametro: string): any {
    if ( busqueda === undefined) return sistemas;
    return sistemas.filter(function(sis){
      //filtro por nombre
      if (parametro === 'Nombre'.toString()) {
        return sis.nombre.toLowerCase().includes(busqueda.toLowerCase());
      //filtro por correo electrónico
      }else if (parametro === 'Pais'.toString()) {
        return sis.pais.toLowerCase().includes(busqueda.toLowerCase());
      //filtro por empresa
      }else if (parametro === 'Provincia'.toString()) {
        return sis.provincia.toLowerCase().includes(busqueda.toLowerCase());
      //filtro por nombre de usuario
      }else if (parametro === 'Canton'.toString()) {
        return sis.canton.toLowerCase().includes(busqueda.toLowerCase());
      //filtro por rol
      }else {
        return sis;
      }
        })
  }
}