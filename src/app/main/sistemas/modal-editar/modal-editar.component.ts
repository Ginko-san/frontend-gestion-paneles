import { Component, OnInit, Input } from '@angular/core';
import { ModalConfirmacionService } from '../../globales/confirmacion-modal/confirmacion-modal.component.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SistemasService } from '../sistemas.service';
import { ToasterManagerService } from '../../../@core/toast/toaster-manager.service';
import { Sistemas } from '../sistemas';

@Component({
  selector: 'ngx-modal-editar',
  templateUrl: './modal-editar.component.html',
  styleUrls: ['./modal-editar.component.scss']
})
export class ModalEditarComponent implements OnInit {

  constructor(private modalConfirmar: ModalConfirmacionService,
     public activeModal: NgbActiveModal, private servicioSistemas: SistemasService,
      private toasterManagerService: ToasterManagerService) { }

  ngOnInit() {
    this.nuevoSistema = new Sistemas();
    this.nuevoSistema = Object.assign({}, this.nuevoSistema, this.sistemaActual)

    if(this.modo=="agregar"){
      this.menuAccion ="Agregar sistema";
    }
    else{
      this.menuAccion = "Editar sistema";
    }

  }
  
  @Input() sistemaActual : Sistemas;
  @Input() listaSistemas;
  @Input() modo;

  menuAccion: any;

  nuevoSistema;


  cerrarModal() {
    this.activeModal.close()
  }

  decisionModoInsertarEditar(){
    if(this.modo=="editar"){
      this.editarDatos();
    }else{
      this.agregarDatos();
    }
  }

  agregarDatos(){
    this.servicioSistemas.insertarSistema(this.nuevoSistema).subscribe(
      sistema=>{
        //este success lo envia desde el backend. Para comprobar si ya existe usuario.
        if(sistema.success==true){
          this.nuevoSistema.id = sistema.id;
          this.listaSistemas.push(this.nuevoSistema);
          //para ordenarlo después de insertarlo.
          this.toasterManagerService.makeToast('success','Éxito','Se ha insertado con éxito');
          this.cerrarModal();
        }
        else{
          this.toasterManagerService.makeToast('error','Error','Ese nombre de sistema ya existe');
        }
      },
      error=>{
        this.toasterManagerService.makeToast('error','No se pudo insertar','No se ha insertado debido a un error en el servidor');
      }
    )
  }


  editarDatos(){
    this.modalConfirmar.confirmar('Confirmar','¿Quieres editar los cambios?')
    .then((confirmed)=>{
      if(confirmed){
        this.servicioSistemas.modificarSistema(this.nuevoSistema).subscribe(
          sistema=>{
            if(sistema.success==false){
              this.toasterManagerService.makeToast('error','No se pudo modificar','Ya existe un sistema con ese nombre');
            }
            else{
              this.toasterManagerService.makeToast('success','Éxito','Se ha modificado con éxito');
              this.sistemaActual.nombre = this.nuevoSistema.nombre;
              this.sistemaActual.pais = this.nuevoSistema.pais;
              this.sistemaActual.provincia = this.nuevoSistema.provincia;
              this.sistemaActual.canton = this.nuevoSistema.canton;
              this.cerrarModal();
            }
          },
          error=>{
            this.toasterManagerService.makeToast('error','No se pudo modificar','No se ha modificado debido a un error en el servidor');
            this.cerrarModal();
          }
        )
      }
    })
  }

}
