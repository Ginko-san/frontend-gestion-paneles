import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-sistemas',
  template: `<router-outlet></router-outlet>`,
})
export class SistemasComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
