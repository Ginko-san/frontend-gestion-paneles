import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MostrarComponent} from './mostrar/mostrar.component';
import { ModalEditarComponent } from './modal-editar/modal-editar.component';
import { SistemasComponent } from './sistemas.component';
const routes: Routes = [  {
  path: '',
  component: SistemasComponent,
  children: [
    {
      path: 'mostrar',
      component: MostrarComponent,
    },
    {
      path:'editar',
      component: ModalEditarComponent,
    },
  ],
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SistemasRoutingModule { }
