import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment.prod'
//import { Usuario, Sistemas, Roles} from './usuario';
//import {Usuario} from '././../../auth/usuario';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Sistemas } from './sistemas';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

@Injectable()
export class SistemasService {
  URL: string = `http://${environment.server}/`;

  constructor(private http: HttpClient) { }

  mostrarSistemas(): Observable<Sistemas[]> {
    let obtenerURL = this.URL.concat('sistemas/obtener');
    return this.http.get<Sistemas[]>(obtenerURL, {responseType:'json'});
  }

  insertarSistema(sistema: Sistemas): Observable<Sistemas> {
    return this.http.post<Sistemas>(this.URL.concat('sistemas/insertar'), sistema, { responseType: 'json' })
  }
  modificarSistema(sistema: Sistemas): Observable<Sistemas> {
    return this.http.put<Sistemas>(this.URL.concat('sistemas/modificar/'+ sistema.id), sistema, { responseType: 'json' })
  }
  eliminarSistema( id: number):Observable<{}>{
    return this.http.delete(this.URL.concat('sistemas/eliminar/' + id), { responseType: 'text' });
  }
}
