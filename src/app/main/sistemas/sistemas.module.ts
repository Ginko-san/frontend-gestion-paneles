import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { InsertarComponent } from './insertar/insertar.component';
import { ThemeModule } from '../../@theme/theme.module';
import { MostrarComponent } from './mostrar/mostrar.component';
//import { NgxPaginationModule } from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';
//import { VerificarFuncionarioDirective } from './verificar-funcionario.directive';
//import { VerificarUsuarioDirective } from './verificar-usuario.directive';
//import { ModalConfirmacionComponent } from '../componentes-globales/modal-confirmacion/modal-confirmacion.component';
//import { ModalConfirmacionService } from '../componentes-globales/modal-confirmacion/modal-confirmacion.service';
import { ToasterManagerService } from '../../@core/toast/toaster-manager.service';
import { ToasterModule } from 'angular2-toaster';
import { MainModule } from '../../main/main.module';
import { ModalConfirmacionService } from '../globales/confirmacion-modal/confirmacion-modal.component.service';
import { ConfirmacionModalComponent} from '../globales/confirmacion-modal/confirmacion-modal.component';
import { ModalEditarComponent } from './modal-editar/modal-editar.component';
import { SistemasComponent } from './sistemas.component';
import { SistemasService } from './sistemas.service';
import { SistemasRoutingModule } from './sistemas-routing.module';
import { FiltroSistemasPipe } from './filtroSistemas.pipe';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    SistemasRoutingModule,
    ThemeModule,
    NgxPaginationModule,
    NgSelectModule,
    ToasterModule.forRoot(),
    MainModule,
  ],
  declarations: [SistemasComponent, MostrarComponent, FiltroSistemasPipe, ModalEditarComponent],
 //   VerificarFuncionarioDirective, VerificarUsuarioDirective],, InsertarComponent
  providers: [SistemasService,ToasterManagerService,ModalConfirmacionService],
  entryComponents: [ConfirmacionModalComponent]  //InsertarComponent
    
    //ModalConfirmacionComponent],
})
export class SistemasModule { }
