import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ToasterManagerService } from '../../../@core/toast/toaster-manager.service';
import { configToasterManager } from '../../../@core/toast/config';
import {ModalConfirmacionService} from '../../globales/confirmacion-modal/confirmacion-modal.component.service';
import { ModalEditarComponent } from '../modal-editar/modal-editar.component';
import { SistemasService } from '../sistemas.service';
import { Sistemas } from '../sistemas';

@Component({
  selector: 'ngx-mostrar',
  templateUrl: './mostrar.component.html',
  styleUrls: ['./mostrar.component.scss']
})
export class MostrarComponent implements OnInit {

  constructor( private confirmacion: ModalConfirmacionService, private modalService: NgbModal,
    private serviciosSistemas: SistemasService, private toasterManagerService: ToasterManagerService) {
  }

  modal : NgbModalOptions = {};
  filtros = ['Nombre', 'Pais', 'Provincia','Canton'];

  seleccionado : any = 'Nombre';
  busqueda: string;
  response: any;
  config = configToasterManager;

  private listaSistemas : Sistemas[];
 // private listaRoles : Roles[];
 // private listaUsuarios : Usuario[];

  obtenerSistemas():void{
    this.serviciosSistemas.mostrarSistemas()
      .subscribe(res => {this.listaSistemas = res},
        error=>{
          this.toasterManagerService.makeToast('error','Error','Error al obtener los sistemas')
        },
      );
  }


  creacionModal(sistemaData: Sistemas){
    this.modal.backdrop = 'static';
    this.modal.keyboard = false;
    const ref = this.modalService.open(ModalEditarComponent)
    ref.componentInstance.listaSistemas = this.listaSistemas;
    if(sistemaData == null){
      ref.componentInstance.modo = "agregar";
    }
    else{
      ref.componentInstance.modo = "editar";
    }
    ref.componentInstance.sistemaActual = sistemaData;
  }

  public ngOnInit() {
    this.obtenerSistemas();
  }

  eliminarSistema(sistema: Sistemas){
    this.confirmacion.confirmar('Confirmación','¿Está seguro que desea eliminar este sistema?')
      .then((confirmado)=>{
        if(confirmado){
          const posicion = this.listaSistemas.findIndex(
          (sis:Sistemas)=>{
            return sis.id === sistema.id;
          },
          );
          this.serviciosSistemas.eliminarSistema(sistema.id).subscribe(
            (data)=>{
              this.listaSistemas.splice(posicion,1),
              this.toasterManagerService.makeToast('success','Éxito','Se ha eliminado con éxito')
            },
          (error) =>{
            this.toasterManagerService.makeToast('error',' Error','Hubo un problema al eliminar el sistema')
          }
          );
          }
        })
    }

/*
  crearModal(usuarioData: Usuario, sistema: string){
    this.modal.backdrop = 'static';
    this.modal.keyboard = false;
    const ref = this.modalService.open(ModalEditarComponent)
    ref.componentInstance.listaUsuarios = this.listaUsuarios;

    //se envia también el nombre del sistema y del rol..
    usuarioData.nombre_sistema = this.encontrarNombreSistema(usuarioData.sistema_id);
    usuarioData.nombre_rol = this.encontrarNombreRol(usuarioData.rol_id);

    ref.componentInstance.usuarioActual = usuarioData;
    ref.componentInstance.listaSistemas = this.listaSistemas;
    ref.componentInstance.listaRoles = this.listaRoles;
  }

  encontrarNombreSistema(idSistema:number){
    for (let sistema of this.listaSistemas){
      if(sistema.id == idSistema){
        return sistema.nombre;
      }
    }
  }

  encontrarNombreRol(idRol:number){
    for (let rol of this.listaRoles){
      if(rol.id == idRol){
        return rol.nombre;
      }
    }
  }


  eliminar(usuario: Usuario){
    this.confirmacion.confirmar('Confirmación','¿Está seguro que desea eliminar este usuario?')
      .then((confirmado)=>{
        if(confirmado){
          const posicion = this.listaUsuarios.findIndex(
            (user:Usuario)=>{
              return user.id === usuario.id;
            },
            );
            this.servicioUsuarios.eliminarUsuario(usuario.id).subscribe(
              (data)=>{
                this.listaUsuarios.splice(posicion,1),
                this.toasterManagerService.makeToast('success','Éxito','Se ha eliminado con éxito')
              },
            (error) =>{
              this.toasterManagerService.makeToast('error',' Error','Hubo un problema al eliminar el usuario')
            }
            );
          }
        })
  }*/
  }
  