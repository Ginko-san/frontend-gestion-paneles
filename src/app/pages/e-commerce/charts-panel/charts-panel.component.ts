import { Component, OnDestroy, ViewChild } from '@angular/core';
import { takeWhile } from 'rxjs/operators';

import { OrdersChartComponent } from './charts/orders-chart.component';
import { ProfitChartComponent } from './charts/profit-chart.component';
import { OrdersChart } from '../../../@core/data/orders-chart.service';
import { ProfitChart } from '../../../@core/data/profit-chart.service';
import { OrdersProfitChartService, OrderProfitChartSummary } from '../../../@core/data/orders-profit-chart.service';
import { DispositivosService } from '../../../main/dispositivos/dispositivos.service';
import { AuthService } from '../../../auth/auth.service';
import { Dispositivos } from '../../../main/dispositivos/dispositivos';

@Component({
  selector: 'ngx-ecommerce-charts',
  styleUrls: ['./charts-panel.component.scss'],
  templateUrl: './charts-panel.component.html',
})
export class ECommerceChartsPanelComponent implements OnDestroy {

  private alive = true;

  chartPanelSummary: OrderProfitChartSummary[];
  
  period: string = 'week';
  dispositivo: any = '--Seleccione un dispositivo--' ;

  ordersChartData: OrdersChart;
  profitChartData: ProfitChart;

  @ViewChild('ordersChart') ordersChart: OrdersChartComponent;
  @ViewChild('profitChart') profitChart: ProfitChartComponent;

  listaDispositivos : Dispositivos[] = [];
  toasterManagerService: any;
  
  constructor(private ordersProfitChartService: OrdersProfitChartService,
              private serviciosDispositivos: DispositivosService,
              private authService: AuthService,) {
    this.ordersProfitChartService.getOrderProfitChartSummary()
      .pipe(takeWhile(() => this.alive))
      .subscribe((summary) => {
        this.chartPanelSummary = summary;
      });

    this.getOrdersChartData(this.period, this.dispositivo);
  //  this.getProfitChartData(this.period);
  }

  setDispositivoGetChartData(value:any){
    if (this.dispositivo !== value.dispositivo) {
      this.dispositivo = value.dispositivo;
    }
 //   console.log(value.period);
  //  console.log(value.dispositivo);
    this.getOrdersChartData(value.period, value.dispositivo);
  }

  setPeriodAndGetChartData(value: any): void {
    if (this.period !== value.period) {
      this.period = value.period;
    }
    this.getOrdersChartData(value.period, value.dispositivo);
  }

  ngOnInit(){
    this.cargarInversor();
  }

  cargarInversor(){
    this.serviciosDispositivos.mostrarDispositivos()
    .subscribe(
      res =>
        { 
          res.forEach(element=>{
            if(element.sistema_id===this.authService.getSistemaId()){
              this.listaDispositivos.push(element);
            }
          })
        /*  if(this.listaDispositivos.length!==0){
            /
              this.dispositivo = this.listaDispositivos[0].deviceid;
          }*/
        },
      error=>{
        this.toasterManagerService.makeToast('error','Error','Error al obtener los dispositivos')
      },
    );
  }

  changeTab(selectedTab) {
    if (selectedTab.tabTitle === 'Profit') {
      this.profitChart.resizeChart();
    } else {
      this.ordersChart.resizeChart();
    }
  }

  //importante
  getOrdersChartData(period: string, dispositivo:any) {
    this.ordersProfitChartService.getOrdersChartData(period,dispositivo)
    //  .pipe(takeWhile(() => this.alive))
      .then(ordersChartData => {
        this.ordersChartData = ordersChartData;
      });
  }


  ngOnDestroy() {
    this.alive = false;
  }
}
