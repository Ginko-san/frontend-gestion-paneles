import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { NbMediaBreakpoint, NbMediaBreakpointsService, NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';
import { DispositivosService } from '../../../../main/dispositivos/dispositivos.service';
import { Dispositivos } from '../../../../main/dispositivos/dispositivos';
import { AuthService } from '../../../../auth/auth.service';
import { OrdersChartService } from '../../../../@core/data/orders-chart.service';


@Component({
  selector: 'ngx-chart-panel-header',
  styleUrls: ['./chart-panel-header.component.scss'],
  templateUrl: './chart-panel-header.component.html',
})
export class ChartPanelHeaderComponent implements OnDestroy {

  private alive = true;

  //estos se envían cada vez que se cambia el periodo o el dispositivo
  @Output() periodChange = new EventEmitter();
  @Output() dispositivoChange = new EventEmitter();

  @Input() type: string = 'week';
  @Input() typeDispositivo;
  //la lista de dispositivos disopnibles
  @Input() dispositivos;

  types: string[] = ['week', 'month', 'year'];
  chartLegend: {iconColor: string; title: string}[];
  breakpoint: NbMediaBreakpoint = { name: '', width: 0 };
  breakpoints: any;
  currentTheme: string;

  
  //private listaDispositivos : Dispositivos[] = [];
  toasterManagerService: any;

  constructor(private themeService: NbThemeService,
              private breakpointService: NbMediaBreakpointsService,
              private serviciosDispositivos: DispositivosService,
              private authService: AuthService,
              private data : OrdersChartService) {

    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        const orderProfitLegend = theme.variables.orderProfitLegend;

        this.currentTheme = theme.name;
        this.setLegendItems(orderProfitLegend);
      });

      this.breakpoints = this.breakpointService.getBreakpointsMap();
      this.themeService.onMediaQueryChange()
        .pipe(takeWhile(() => this.alive))
        .subscribe(([oldValue, newValue]) => {
          this.breakpoint = newValue;
        });
  }

  ngOnInit() {
  //  this.cargarInversor();
  }

  
  changeDispositivo(dispositivo: number): void {
    this.typeDispositivo = dispositivo.toString();
    this.dispositivoChange.emit({period:this.type, dispositivo:this.typeDispositivo});
  }

  setLegendItems(orderProfitLegend) {
    this.chartLegend = [
      {
        iconColor: orderProfitLegend.secondItem,
        title: 'Power',
      }
    ];
  }

  changePeriod(period: string): void {
    this.type = period;
    this.periodChange.emit({period:this.type, dispositivo:this.typeDispositivo});
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
