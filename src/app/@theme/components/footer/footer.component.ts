import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Creado por <b>Miguel Angel Méndez Rojas</b> 2018</span>
    <div class="socials">
      <a href="https://github.com/miguelmendez17" target="_blank" class="ion ion-social-github"></a>
      <a href="https://www.facebook.com/miguelangel.mendezrojas.5" target="_blank" class="ion ion-social-facebook"></a>
      <a href="https://twitter.com/MiguelMendezRo1" target="_blank" class="ion ion-social-twitter"></a>
      <a href="#" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
    <span class="created-by">Modulo de predicción por <b>Andrés de Jesús García Salas</b> 2019</span>
    <div class="socials">
      <a href="https://github.com/Ginko-san" target="_blank" class="ion ion-social-github"></a>
      <a href="https://www.facebook.com/GinkoSans" target="_blank" class="ion ion-social-facebook"></a>
      <a href="https://twitter.com/Ginkosans" target="_blank" class="ion ion-social-twitter"></a>
      <a href="https://www.linkedin.com/in/agarciasalas/" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
  `,
})
export class FooterComponent {
}
