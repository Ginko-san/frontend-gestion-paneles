import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit , Input} from '@angular/core';

@Component({
  selector: 'ngx-confirmacion-modal',
  templateUrl: './confirmacion-modal.component.html',
  styleUrls: ['./confirmacion-modal.component.scss']
})
export class ConfirmacionModalComponent implements OnInit {

  @Input() titulo: string;
  @Input() mensaje: string;
  @Input() btnOkText: string;
  @Input() btnCancelText: string;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
  public cancelar() {
    this.activeModal.close(false);
  }

  public aceptar() {
    this.activeModal.close(true);
  }

  public dismiss() {
    this.activeModal.dismiss();
  }
}
