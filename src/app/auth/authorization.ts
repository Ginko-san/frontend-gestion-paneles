export interface Authorization {
  token: string;
  expiracion: number;
}
