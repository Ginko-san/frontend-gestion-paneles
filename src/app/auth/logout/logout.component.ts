import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';

@Component({
  selector: 'ngx-logout',
  template: ``,
})
export class LogoutComponent implements OnInit {

  //el servicio de Auth es el que tiene la función de cerrar sesión con
  //credenciales.
  constructor(private serviceAuth: AuthService) { }

  ngOnInit() {
    //cuando entra a este componente, llama al auth service 
    //para eliminar información de auth y lo dirige al login
    this.serviceAuth.cerrarSesion();
  }

}
