import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../auth.service';
import {configToasterManager} from '../../@core/toast/config';
import 'style-loader!angular2-toaster/toaster.css';
import {Router} from '@angular/router';
import {ToasterManagerService} from '../../@core/toast/toaster-manager.service';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  configuration = configToasterManager;
  usuario: any = {};

  constructor(private authService: AuthService, private router: Router,
               private toasterManagerService: ToasterManagerService) {}

  onSubmit(form: NgForm) {
    // valida si los datos del formulario son validos
    if (form.valid) {
      // intenta loguear en el sistema
      this.authService.login(form.value).subscribe(
        // si fue exitoso setea la informacion del usuario logueado
        (data) => {
          localStorage.clear();
          if(data.success == true){
            //se guarda el token en el local storage
          localStorage.setItem('token', data.token);

          //se guardar datos de autenticación
          this.authService.guardarDatosAuth(data);
          //se redirigie al main
          this.router.navigate(['/main']);
          }
          //el data viene con un error. Aquí muestra el error que trae el mensaje.
          else{
            this.toasterManagerService.makeToast('error','Error',data.mensaje);
          }
        },
        //este es un error de conexión con el servidor
        (error) => {
          switch (error.status) {
            case 0: {
              this.toasterManagerService.makeToast('error', 'Error'
                , 'Error en la conexion con el servidor.');
              break;
            }
            case 401: {
              this.toasterManagerService.makeToast('error', 'Error'
                , 'No existe un usuario con las credenciales ingresadas.');
              break;
            }
            default: {

              break;
            }
          }
        },
      );
    }
  }
}
