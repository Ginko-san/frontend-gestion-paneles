import {Usuario} from './usuario';

export class Auth {
  private _id: number;
  private _usuario: string;
  private _rol_id: number;
  private _sistema_id: number;
  private _success: boolean;
  private _token: string;
  private _mensaje: string;
  private _nombre_sistema: string;
 
  public get nombre_sistema(): string {
    return this._nombre_sistema;
  }
  public set nombre_sistema(value: string) {
    this._nombre_sistema = value;
  }
  
  public get mensaje(): string {
    return this._mensaje;
  }
  public set mensaje(value: string) {
    this._mensaje = value;
  }

  public get token(): string {
    return this._token;
  }
  public set token(value: string) {
    this._token = value;
  }

  get id(): number{
    return this._id;
  }

  set id(value:number){
    this._id = value;
  }

  get usuario(): string{
    return this._usuario;
  }

  get rol_id(): number {
    return this._rol_id;
  }

  set rol_id(value: number) {
    this._rol_id = value;
  }


  get success(): boolean{
    return this._success;
  }

  set success(value:boolean){
    this._success = value;
  }

  get sistema_id(): number {
    return this._sistema_id;
  }
  set sistema_id(value: number) {
    this._sistema_id = value;
  }

}
