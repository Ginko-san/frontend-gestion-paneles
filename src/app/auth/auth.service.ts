import {Injectable} from '@angular/core';
import {Usuario} from './usuario';
import {Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Authorization} from './authorization';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import {Auth} from './auth';
import {ToasterManagerService} from '../@core/toast/toaster-manager.service';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../environments/environment.prod';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable()
export class AuthService {
  private cachedRequests: Array<HttpRequest<any>> = [];
  /**
   * @type {BehaviorSubject<Auth>}
   * contiene la informacion del usuario que se logueo en el sistema el id,
   * los datos que este posee como funcionario
   */
  private authorization = new BehaviorSubject<Authorization>(null);
  private auth = new BehaviorSubject<Auth>(null);

  constructor(private router: Router, private httpClient: HttpClient,
              private toasterManagerService: ToasterManagerService) {
  }

  /**
   *
   * @returns {Authorization}
   */
  getAuthorization(): Authorization {
    return this.authorization.getValue();
  }

  /**
   *
   * @param {Authorization} authorization
   */
  setAuthorization(authorization: Authorization) {
 //   console.log(authorization.token+'token');
  //  console.log(authorization.expiracion);
    this.authorization.next(authorization);
  }

  /**
   *
   * @returns {number}
   */
    getUserId(): number {
    return this.auth.getValue().id;
  }

  /**
   *
   * @returns {string}
   */
   getUsuario(): string {
    return this.auth.getValue().usuario;
  }

  /**
   *
   * @returns {BehaviorSubject<Auth>}
   */
  getAuth(): BehaviorSubject<Auth> {
    return this.auth
  }

  /**
   *
   * @param {Auth} auth
   */
  setAuth(auth: Auth) {
    this.auth.next(auth);
  }

  /**
   *
   * @returns {number}
   */
  getRol(): number {
    return this.auth.getValue().rol_id;
  }

   /**
   *
   * @returns {string}
   */
  getNombreSistema(): string {
    return this.auth.getValue().nombre_sistema;
  }
  
  /**
   *
   * @returns {number}
   */
  getSistemaId(): number {
    return this.auth.getValue().sistema_id;
  }

  /**
   *
   * @param {Usuario} usuario
   * @returns {Observable<Auth>}
   */
  login(usuario: Usuario): Observable<Auth> {
    return this.httpClient.post<Auth>(`http://${environment.server}/login`, usuario, httpOptions);
  }

  //función que guarda los datos de autenticación de inicio de sesión.
  guardarDatosAuth(data): void {
    this.setAuthorization(data);
    this.setAuth(data)
  }
  /**
   *
   * @returns {any}
   */
  refreshToken(): any {
   // this.httpClient.post<Authorization>(`http://${environment.server}/api/auth/refresh`, null, httpOptions)
    this.httpClient.post<Authorization>(`http://localhost/api/auth/refresh`, null, httpOptions)
      .subscribe(
        (data) => {
          this.setAuthorization(data);
        },
        (error) => console.error(error),
    );
  }

  /**
   * Se dirige al login y borra información de autentificación.
   */
  cerrarSesion() {
    this.authorization.next(null);
    this.auth.next(null);
    this.router.navigate(['auth/login']);
    localStorage.clear();
  }

  public collectFailedRequest(request): void {
    this.cachedRequests.push(request);
  }

  public retryFailedRequests(): void {
    // retry the requests. this method can
    // be called after the token is refreshed
  }
}
