export class Usuario {
  id: number;
  rol_id: number;
  usuario: string;
  contrasenna: string;
  nombre: string;
  correo: string;
  sistema_id: number;
  created_at: Date;
  updated_at: Date;


  constructor(id: number, rol:number, usuario:string, contrasenna:string, nombre:string,
              correo: string, sistema_id:number,created_at: Date, updated_at: Date) {
    this.id =  id;
    this.rol_id = rol;
    this.usuario = usuario;
    this.contrasenna = contrasenna;
    this.nombre = nombre;
    this.correo = correo;
    this.sistema_id = sistema_id;
    this.created_at = created_at;
    this.updated_at = updated_at;
  }
}



